'use strict';

const express = require('express');
const app = express();
const Dao = require('./dao.js');

app.get('/api/contacts', getContacts);
app.get('/api/contacts/:id', getContact);

var url = 'mongodb://user1:12345@ds011860.mlab.com:11860/i399';
var dao = new Dao();

// Ühendus avatakse
dao.connect(url)
    .then(() => app.listen(3000));


function getContacts(request, response) {
    response.set('Content-Type', 'application/json');

    dao.findAll()
        .then(data => {
            response.end(JSON.stringify(data));
        }).catch(error => {
            console.log(error);
            response.end('error: ' + error);
        });

}

function getContact(request, response) {
    var id = request.params.id;
    response.set('Content-Type', 'application/json');
    response.end(JSON.stringify({id: id}));
}

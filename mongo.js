'use strict';

const mongodb = require('mongodb');
const Dao = require('./dao.js');
const ObjectID = mongodb.ObjectID;

var url = 'mongodb://user1:12345@ds011860.mlab.com:11860/i399';

var data = {name: 'Jared'};

// insert();

// findById('591e26f8c2717a2352a5e7f3')
//     .then(data => console.log(data));

// findAll()
//     .then(data => console.log(data));

var dao = new Dao();

dao.connect(url)
    .then(() => {
        return dao.findById('591e26f8c2717a2352a5e7f3');
    }).then(data => {
        dao.close();
        console.log(data);
    }).catch(error => {
        dao.close();
        console.log('Error: ' + error)
    });

function findById(id) {
    var database;
    return mongodb.MongoClient.connect(url).then(db => {
        database = db;
        id = new ObjectID(id);
        return db.collection('contacts').findOne({_id: id});
    }).then(data => {
        closeDb(database)
        return data;
    }).catch(error => {
        closeDb(database);
        throw error;
    });
}

function findAll() {
    var database;
    return mongodb.MongoClient.connect(url).then(db => {
        database = db;
        return db.collection('contacts').find().toArray();
    }).then(data => {
        closeDb(database)
        return data;
    }).catch(error => {
        closeDb(database);
        throw error;
    });
}

function insert() {
    var database;
    return mongodb.MongoClient.connect(url).then(db => {
        database = db;
        return db.collection('contacts').insertOne(data);
    }).then(() => {
        closeDb(database)
    }).catch(error => {
        closeDb(database);
        throw error;
    });
}


function closeDb(database) {
    if (database) {
        database.close();
    }
}